<?php


namespace App\Presenters;


use Nette\Application\AbortException;
use Nette\Application\UI\Form;

class FormPresenter extends BasePresenter {

	/** @var \App\Model\FormManager @inject */
	public $formManager;

	public function makeBootstrap4( Form $form ): void {
		$renderer                                        = $form->getRenderer();
		$renderer->wrappers['controls']['container']     = null;
		$renderer->wrappers['pair']['container']         = 'div class="form-group row"';
		$renderer->wrappers['pair']['.error']            = 'has-danger';
		$renderer->wrappers['control']['container']      = 'div class=col-sm-9';
		$renderer->wrappers['label']['container']        = 'div class="col-sm-3 col-form-label"';
		$renderer->wrappers['control']['description']    = 'span class=form-text';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=form-control-feedback';
		foreach ( $form->getControls() as $control ) {
			$type = $control->getOption( 'type' );
			if ( $type === 'button' ) {
				$control->getControlPrototype()->addClass( empty( $usedPrimary ) ? 'btn btn-primary' : 'btn btn-secondary' );
				$usedPrimary = true;
			} elseif ( in_array( $type, [ 'text', 'textarea', 'select' ], true ) ) {
				$control->getControlPrototype()->addClass( 'form-control' );
			} elseif ( $type === 'file' ) {
				$control->getControlPrototype()->addClass( 'form-control-file' );
			} elseif ( in_array( $type, [ 'checkbox', 'radio' ], true ) ) {
				if ( $control instanceof Nette\Forms\Controls\Checkbox ) {
					$control->getLabelPrototype()->addClass( 'form-check-label' );
				} else {
					$control->getItemLabelPrototype()->addClass( 'form-check-label' );
				}
				$control->getControlPrototype()->addClass( 'form-check-input' );
				$control->getSeparatorPrototype()->setName( 'div' )->addClass( 'form-check' );
			}
		}
	}

	public function createComponentContactForm() {
		$form             = new Form();
		$form->onRender[] = array( $this, 'makeBootstrap4' );
		$form->addText( 'name', 'Jméno a příjmení' )->setRequired( 'Vyplňte prosím pole Vaše jméno.' );
		$form->addText( 'email',
			'Váš email' )->setType( 'mail' )->setRequired( 'Vyplňte prosím pole Váš email' )->addRule( Form::EMAIL,
			'Zadejte prosím platný email.' );
		$form->addTextArea( 'content', 'Váš vzkaz' );
		$form->addReCaptcha(
			'captcha', // control name
			'Zabiják robotů', // label
			"Prosím, potvrďte, že nejste robot." // error message
		);
		$form->addSubmit( 'send', 'Odeslat' );
		$form->onSuccess[] = array( $this, 'saveData' );

		return $form;
	}

	public function saveData( $form, $values ) {
		$this->formManager->saveData( $values );
		$this->flashMessage('Záznam byl úspěšně uložen','success');
		$this->redirect( 'Form:messages' );

	}

	public function renderMessages(int $page = 1){
		$messages = $this->formManager->getData();
		$lastPage = 0;
		$this->template->messages = $messages->page($page, 10, $lastPage);
		$this->template->page = $page;
		$this->template->lastPage = $lastPage;
	}

	public function actionRemove($id){
		if($id){
			$this->formManager->removeData($id);
			$this->flashMessage('Záznam byl úspěšně odstraněn','success');
			$this->redirect( 'Form:messages' );
		}
	}
}
<?php


namespace App\Model;


use Nette\Database\Context;
use Nette\Database\Table\Selection;

class FormManager {

	protected $database;

	public function __construct( Context $database ) {
		$this->database = $database;
	}

	public function saveData( $data ) {
		$email = trim( $data->email );
		$email = strtolower( $email );
		$email = md5( $email );
		$data->hash = $email;
		$this->database->table( 'messages' )->insert( $data );
	}

	public function getData(): Selection
	{
		return $this->database->table( 'messages' )->order('date DESC');
	}

	public function removeData($id){
		return $this->database->table( 'messages' )->wherePrimary($id)->delete();
	}
}